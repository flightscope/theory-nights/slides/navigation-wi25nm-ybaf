## Format { data-transition="fade none" }

* **1800-1900** Navigation theory part 1
* **1900-1915** Break
* **1915-2015** Navigation theory part 2
* **2015-2030** Open question time

---

## These slides { data-transition="fade none" }

<div style="text-align: center; font-size: large">
  [https://flightscope.gitlab.io/theory-nights/slides/navigation-wi25nm-ybaf/](https://flightscope.gitlab.io/theory-nights/slides/navigation-wi25nm-ybaf/)
</div>

