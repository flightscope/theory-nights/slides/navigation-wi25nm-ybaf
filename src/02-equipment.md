## Helpful Equipment { data-transition="fade none" }

<p style="text-align: center">
  <a href="images/theory-nights/vtc-wacruler-protractor.jpg">
    <img src="images/theory-nights/vtc-wacruler-protractor.jpg" alt="Brisbane VTC, WAC ruler, protractor" width="600px"/>
  </a>
</p>

* Brisbane Visual Terminal Chart (VTC)
* WAC ruler
* Protractor

---

## Helpful Equipment { data-transition="fade none" }

###### En Route Supplement Australia (ERSA)

* Can be purchased as a paper book
* Can be viewed online under the AIP
  * [https://www.airservicesaustralia.com/aip/aip.asp](https://www.airservicesaustralia.com/aip/aip.asp)
  * *(so can the Brisbane VTC from the same link)*
  * There will be references to the AIP within these slides
  * Bookmark this link!
* Can be viewed within several Electronic Flight Bags (EFBs)
  * AvPlan
  * Ignition Aerospace
  * OzRunways

---

