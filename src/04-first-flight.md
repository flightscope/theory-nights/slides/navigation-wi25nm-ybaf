## Scenario { data-transition="fade none" }

* You have earned your RPC and passenger endorsement with blood, sweat and money
* You are planning to take your friend flying in the Eurofox!
* Your intended flight route is:
  * Archerfield, departing east
  * Macleay Island
  * Dunwich
  * Heckfield
  * Park Ridge Water Tower
  * Archerfield

---

## Scenario

* First, let's find (and mark) these points on our VTC
* Using a WAC ruler, measure the distance from YBAF to each of our points
* Also note the grid markings on the VTC, which can be used to approximate distance

---

## RAAus Operations Manual 2.01(5)

<p style="text-align: center">
  <a href="images/theory-nights/raaus-25nm.png">
    <img src="images/theory-nights/raaus-25nm.png" alt="RAAus Operations Manual 2.01(5)" width="600px"/>
  </a>
</p>

<div style="font-size:large">
  Is our flight operation within our navigation legal limits?
</div>

<div style="font-size:small">
  *the RAAus Operations Manual can be found online (pdf) by typing into a search engine*
</div>

---

## Flight Plan Waypoints

* Open the ERSA to
  * `IND - GEN - 1 (AERODROMES AND ALA CODES ENCODED)`
* Write down our planned route using the appropriate codes

---

## Flight Plan Waypoints

<div style="text-align: center">
  YBAF - YXMC - YDUN - YHEC - PKR - YBAF
</div>

---

## Our first leg

###### YBAF - YXMC

* On the VTC, we can see that YBAF is a Class D aerodrome SFC/1500
* Discussion point: what does this mean?
* Open the ERSA to
  * `FAC - YBAF - 1 (Brisbane/Archerfield)`
* What is relevant to our intended departure to the east?
  * What are our tracking requirements?
  * Is the control tower available at all times?
  * At what altitude will we depart?

---

## Our first leg

###### YBAF - YXMC

* Draw a line on your VTC from YBAF to the Gateway Motorway and Pacific Highway intersection
* Note that we are *"clear of the AF control zone"* at this intersection
* Using a WAC ruler, measure the **distance** from YBAF to this intersection
* Now, using a protractor, measure the **track** from YBAF to this intersection

---

## Our first leg

###### YBAF - YXMC

* Let's record a planned **distance** of 5.1nm from YBAF to the highway intersection
* However, the **track** we measured is not `088` as the ERSA requires
* Why is this?

---

## Our first leg

######  Magnetic Variation

* Aeronautical charts put north at the *geographic north pole*
* We call this **True North**
* A magnetic compass does not necessarily point to true north
* The difference between **Magnetic North** and **True North** is called *magnetic variation*
* Magnetic Variation differs depending on where you are in the world!

---

## Our first leg

######  Magnetic Variation

* Our onboard compass and heading indicator references Magnetic North
* The tracking requirement in the ERSA is `088M`
* In our local area, the magnetic variation is 10.5°E
* **Record your track using a reference of Magnetic North by converting true to magnetic**
* *See your BAK textbook for more information about magnetic variation*

---

## Our first leg

###### YBAF - YXMC

* Next draw a line and measure the track and distance from the highway intersection to YXMC
* Using the VTC, find the blue lines that indicate airspace boundaries
* We can *not* enter Class C airspace without clearance (VCA!)
* Discussion: what does `C LL 2000` mean?

---

## Our first leg

###### YBAF - YXMC

* We need to cross water on this leg
* Can we fly this leg at 500ft AMSL? Why or why not?
* Let's have a look at
  * *Civil Aviation Order 20.11*
  * *Civil Aviation Regulation 1988 REG 157*
* What are the yellow areas on the VTC?

---

## Our first leg

###### YBAF - YXMC

* Using the VTC, find the green boundaries that indicate radio frequencies
* Which radio frequency will we be monitoring on this leg?
* Will we be passing any **danger areas** on this leg?
* You will find details on these in the ERSA
  * `PRD AREA (PROHIBITED, RESTRICTED, DANGER AREA)`

---

## Our second leg

###### YXMC - YDUN

* On our next leg, we will be flying up the west coast of North Stradbroke Island
* What altitude might we use for this leg of the flight?
* Look up `FAC - YDUN - 1 (DUNWICH)` in the ERSA
  * What can we learn about Dunwich by the ERSA information?
  * Is it a controlled or uncontrolled (UNCR) aerodrome?
  * What is the CTAF?
  * Let's learn this information on the ground, why?
    * Forced landing?
    * Weather turns bad &mdash; prec-search?

---

## Our second leg

###### YXMC - YDUN

* What does CTAF mean?
  * Let's look up *CAAP 166-01 Operations in the vicinity of non-controlled aerodromes*
* There might be other aircraft in the area of YDUN
  * what actions or tasks might we consider to increase Situational Awareness?
  * would we transmit anything on the radio?

---

## Our second leg

###### YXMC - YDUN

* Like most radio call formats
  * who are we calling?
  * who are we?
  * where are we?
  * what are our intentions?

---

## Our second leg

###### YXMC - YDUN

<div style="text-align: center; font-size: large">
  AIP ENR 1.1 (10.1.14 and 10.1.15)
</div>
<p style="text-align: center">
  <a href="images/theory-nights/aip-enr1.1-10.1.14_15.png">
    <img src="images/theory-nights/aip-enr1.1-10.1.14_15.png" alt="AIP ENR 1.1 (10.1.14 and 10.1.15)" width="600px"/>
  </a>
</p>

---

## Our second leg

###### YXMC - YDUN

<div style="text-align: center; font-size: large">
  126.70MHz
</div>

<div style="font-size: xx-large">
> Dunwich Traffic  
> Eurofox fifty tree fifty  
> Overhead the field maintaining too tousand fife hundred  
> Heading of wun niner zero, tracking for Heckfield  
> Dunwich
</div>

---

## Our second leg

###### YXMC - YDUN

* We've heard this (or similar) many times on our training flights
* While we are monitoring Brisbane Centre frequency
* That's because **the pilot forgot to change to the CTAF!**
* Let's not be that pilot :)

---

## Our third leg

###### YDUN - YHEC

* What is the **track** and **distance** from YDUN to YHEC?
* Look up `FAC - YHEC - 1 (HECK FIELD)` in the ERSA
  * There is a *lot* of traffic in/out of YHEC
  * What is the CTAF?
  * What runways does YHEC have?
  * What altitude might we choose to overfly YHEC?
  * What would our radio broadcast be on the CTAF?
  * What does HJ mean?

---

## Our fourth leg

###### YHEC - PKR

* We turn on to a heading for Park Ridge Water Tower
* Pointing out the local sights to our friend
* "There's Mount Stapylton, and if you look out your right window, there's Brisbane City"
* What **track** and **distance** will we fly to PKR?
* Approximately how long will this take us (assuming nil wind)?
* What preparations can we make before arriving at PKR?

---

## Our fourth leg

###### YHEC - PKR

* We will continue monitoring Heckfield CTAF until we are 5nm out
* Then switch back to monitoring Brisbane Centre
* Let's put the YBAF ATIS frequency on stand-by in preparation
* Let's *"stay ahead of the aeroplane"* so that work load does not become too much at once

---

## Our fourth leg

###### YHEC - PKR

<div style="text-align: center; font-size: large">
  Park Ridge Water Tower *(from the south)*
</div>
<p style="text-align: center">
  <a href="images/theory-nights/park-ridge-water-tower.png">
    <img src="images/theory-nights/park-ridge-water-tower.png" alt="Park Ridge Water Tower" width="350px"/>
  </a>
</p>
<div style="text-align: center; font-size: small">
  Source: Archerfield Visual Pilot Guide 2010
</div>

---

## Our last leg

###### PKR - YBAF

* What is the **track** and **distance** from PKR to YBAF?
* Look up `FAC - YBAF - 1 (Brisbane/Archerfield)` in the ERSA
  * Find the heading **ARRIVALS**
  * At what altitude must we enter the Archerfield Control Zone?
  * What do we do if we don't know the inbound frequency to use?
  * What Restricted Area is nearby PKR?
    * ERSA: `PRD AREA (PROHIBITED, RESTRICTED, DANGER AREA)`

---

## Full-stop YBAF

* Your passenger thanks you for their first flight in a Eurofox
* You also receive remarks on how professional and confident you were with your flight
* You pack up, head home and later think to yourself, "yeah &hellip; I guess I did do a great job today"

---

## Overall considerations

###### YBAF-YXMC-YDUN-YHEC-PKR-YBAF

* What is the total **distance** of your flight?
* What is the expected **time** of your flight (assume nil wind)?
* What is the expected total fuel consumption of your flight?

---

## Overall considerations

###### YBAF-YXMC-YDUN-YHEC-PKR-YBAF (Fuel planning)

* What is the **required** fuel reserve for your flight?
  * Let's look up *CAAP 234-1(2.1)*
  * Does Flightscope Aviation have an additional operational requirement?
* It is a good idea to:
  1. Note the time of your departure from YBAF
  2. Calculate and note the time you **must** be back on the ground before reaching fuel reserve
  3. Review fuel consumption and remaining fuel endurance (in time), *at least every 30 minutes*
* What are we going to do if we inadvertently reach fuel reserve?

---

## Overall considerations

###### YBAF-YXMC-YDUN-YHEC-PKR-YBAF (Fuel planning)

<div style="text-align: center; font-size: large">
  "MAYDAY MAYDAY MAYDAY FUEL"
</div>
<p style="text-align: center">
  <a href="images/theory-nights/caap-234-1_6.5.png">
    <img src="images/theory-nights/caap-234-1_6.5.png" alt="CAAP 234-1 (6.5)" width="800px"/>
  </a>
</p>
<div style="text-align: center; font-size: small">
  Source: CAAP 234-1(2.1) section 6.5
</div>

---

## Overall considerations

###### YBAF-YXMC-YDUN-YHEC-PKR-YBAF

* The Archerfield control tower does not operate 24/7
* When closed, we will hear on the ATIS, *"information Zulu"* 
* Suppose we arrive back to YBAF a little later than expected
* The tower is closed
* What are we going to do for our arrival?

---

## Overall considerations

###### YBAF-YXMC-YDUN-YHEC-PKR-YBAF (Meteorology)

* If you do not have an account already, create one on airservices NAIPS
* NAIPS can be accessed by
  * Web browser [https://www.airservicesaustralia.com/naips/](https://www.airservicesaustralia.com/naips/)
  * TrackDirect Android app **_(free)_**
  * AvPlan Android/Apple **_($)_**
  * OzRunways NAIPS on the Apple app store **_($)_**

---

## Overall considerations

###### YBAF-YXMC-YDUN-YHEC-PKR-YBAF (Meteorology)

* Request a location briefing for YBAF, YBBN and YBCG
* Let's have a look at the current METAR/SPECI, TAF and TTF
* Is this current and forecast weather acceptable to conduct your flight?

---

## Overall considerations

###### YBAF-YXMC-YDUN-YHEC-PKR-YBAF (Meteorology)

* There may be terms in the met report that you do not understand
* These can be found in AIP GEN 3.5
* Also, some are in the VFR Guide *(which is available online)*
* If you are ever unsure, *ask an instructor to help you*
* All instructors at Flightscope want you to have a safe and enjoyable time flying

---

