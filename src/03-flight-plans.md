## General Overview { data-transition="fade none" }

* We are going to make plans to conduct flights as Pilot in Command
* We will consider and discuss the factors that relate to our planning
* These are the factors that your instructor considers before your training flights!

---

## A Brief Note { data-transition="fade none" }

* This theory night does not substitute for formal navigation instruction
* Tonight, we will discuss many of the factors that relate to conducting and navigating a safe flight within 25nm of YBAF
* Your instructor may advise you of *other factors* that relate to any intended flight
* Also remember, if you are unsure about anything before your flight, **please ask an instructor**
* Being proactive in seeking out information during our flight training is an important responsibility of piloting
* Also, have fun :)

