## A note on Flightscope Aviation culture { data-transition="fade none" }

* Flightscope Aviation adopts a culture among instructors, students and the general public
* Your flight training is important to you, and to Flightscope
* Achieving your aviation goals requires a significant study effort from the student
* Flightscope instructors are there to support you to achieve these goals

---

## Flightscope Aviation culture { data-transition="fade none" }

###### What does this mean?

* If you ever feel the need to do more study at home, *talk to your instructor and ask for it*
* Alternatively, talk to the Chief Flying Instructor *(Rod Flockhart)*
* If you are ever stuck on a problem, need help, have questions, *please ask an instructor*
* This is as important to you as it is to Flightscope Aviation, and is an important component of a maturing pilot
* **Make sure you are having fun and always learning**

