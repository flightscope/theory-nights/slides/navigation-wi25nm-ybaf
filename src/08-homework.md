## Homework { data-transition="fade none" }

* Using the ERSA, find the abbreviations for
  * Walter Taylor Bridge
  * Kagaru
* Using the Brisbane VTC, find
  * two parachute jumping drop zones
  * Jimboomba golf course
  * Why are there golf courses on the VTC?
* Using the ERSA and the Brisbane VTC, find
  * Danger area D629E and the reason for being a danger area
* What is the restricted area that is 4nm south of YBAF?
  * Is clearance available?
  * Find it in the ERSA (PRD AREA)
* When you are at Dunwich, what is the airspace limitation before violating class C?
* If you were to depart from Watts Bridge (YWSG), what would your radio frequency(s) be?
* What is the direct magnetic track and distance from Heckfield (YHEC) to Target?
* What is the name of the two major roads that intersect 3.5 nautical miles north of Park Ridge Water Tower?
* Read *Civil Aviation Advisory Publication (CAAP) 166-01 Operations in the vicinity of non-controlled aerodromes*
* Dream of your first licenced flight in command, then plan the entire flight using what you have learned at Flightscope Aviation theory night

