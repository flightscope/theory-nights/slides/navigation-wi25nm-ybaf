## Plan your own flight { data-transition="fade none" }

* Let's plan another flight
* Your turn!
* You route will be:
  * YBAF (western departure)
  * Goodna (GON)
  * Yarrabilba
  * Mt Cotton (MCOO)
  * Target (TAR)
  * YBAF

---

## Plan your own flight { data-transition="fade none" }

###### YBAF-GON-Yarrabilba-MCOO-TAR-YBAF

* Measure **magnetic track** and **distances** on each leg
* Fuel planning
* Arrival/Departure procedures
* Airspace requirements
* Altitude selection on each leg
* Prohibited, Restricted and Danger Areas (PRD)
* Any CTAF requirements
* Meteorology as it relates to the flight plan

