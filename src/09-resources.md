## Resources { data-transition="fade none" }

###### These slides

<div style="text-align: center; font-size: x-large">
[https://flightscope.gitlab.io/theory-nights/slides/navigation-wi25nm-ybaf/](https://flightscope.gitlab.io/theory-nights/slides/navigation-wi25nm-ybaf/)
</div>

---

## Resources { data-transition="fade none" }

###### The Aeronautical Information Package (AIP)

<div style="text-align: center; font-size: x-large">
*includes the ERSA and other documentation*

[http://www.airservicesaustralia.com/aip/aip.asp](http://www.airservicesaustralia.com/aip/aip.asp)
</div>

---

######  Airservices NAIPS

* Local and Area weather briefing
* First light/last light
* Flight Notification form
* *etc*

<div style="text-align: center; font-size: x-large">
[https://www.airservicesaustralia.com/naips/](https://www.airservicesaustralia.com/naips/)
</div>

---

###### Flightscope Aviation repository for Archerfield documentation

* Extracts of the AIP
  * Enroute Supplement of Australia (ERSA) entries for Archerfield
  * Brisbane/Sunshine Coast Visual Terminal Chart (VTC)
  * Brisbane Visual Navigation Chart (VNC)
* Archerfield Visual Pilot Guide 2010

<div style="text-align: center; font-size: x-large">
[https://gitlab.com/flightscope/training/archerfield/](https://gitlab.com/flightscope/training/archerfield/)
</div>

---

###### The Flightscope Aviation Aircraft Flight Manual (PoH) repository

<div style="text-align: center; font-size: x-large">
[https://gitlab.com/flightscope/aircraft/manual/](https://gitlab.com/flightscope/aircraft/manual/)
</div>

---

###### The CASA VFR Guide


<div style="text-align: center; font-size: x-large">
contains information for pilots, presented in an accessible format, about rules, procedures, meteorology, flight planning and other subjects

[https://vfrg.casa.gov.au/](https://vfrg.casa.gov.au/)
</div>

